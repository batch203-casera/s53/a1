import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Error from "./pages/Error";

function App() {
  return (
    <div className="App">
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/register" element={<Register />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </div>
  );
}

export default App;
