import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect } from 'react';
function Register() {
    const [isActive, setIsActive] = useState(false);
    const [formData, setFormData] = useState({
        fName: "",
        lName: "",
        email: "",
        mobileNo: "",
        password1: "",
        password2: ""
    });

    function handleChange(event) {
        const { name, value } = event.target;
        setFormData(prevFormData => {
            return {
                ...prevFormData,
                [name]: value
            }
        });
    }

    useEffect(() => {
        if (
            formData.fName.length > 0 &&
            formData.lName.length > 0 &&
            formData.email.length > 0 &&
            formData.mobileNo.length > 0 &&
            formData.password1.length > 0 &&
            formData.password2.length > 0 &&
            formData.password1 === formData.password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [formData]);


    function registerUser(event) {
        event.preventDefault();
        setFormData({
            fName: "",
            lName: "",
            email: "",
            mobileNo: "",
            password1: "",
            password2: ""
        });

        alert('Successfully registered!');
    }
    return (
        <>
            <h1 className='my-5 text-center'>Register</h1>
            <Form className="d-flex flex-column" onSubmit={registerUser}>
                <Form.Group className="mb-3" controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter First Name"
                        name="fName"
                        value={formData.fName}
                        onChange={handleChange}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your name with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Last Name"
                        name="lName"
                        value={formData.lName}
                        onChange={handleChange}
                        required

                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email"
                        placeholder="Enter email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        required />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNumber">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type="text" placeholder="+63<10 digit number> or 0<10 digit number>"
                        name="mobileNo"
                        value={formData.mobileNo}
                        onChange={handleChange}
                        required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" name="password1"
                        value={formData.password1}
                        onChange={handleChange} required />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Re-enter Password" name="password2"
                        value={formData.password2}
                        onChange={handleChange} required />
                </Form.Group>

                <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" className="col-2" disabled={!isActive}>
                    Login
                </Button>   

            </Form>
        </>
    );
}

export default Register;