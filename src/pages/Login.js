import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect } from 'react';
function Register() {


    const [isActive, setIsActive] = useState(false);
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });

    function handleChange(event) {
        const { name, value } = event.target;
        setFormData(prevFormData => {
            return {
                ...prevFormData,
                [name]: value
            }
        });
    }

    useEffect(() => {
        if (
            formData.email.length > 0 &&
            formData.password.length > 0
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [formData]);


    function loginUser(event) {
        event.preventDefault();
        localStorage.setItem("email", formData.email);
        setFormData({
            email: "",
            password: "",
        });
        alert('You are now Logged In!');
    }

    return (
        <>
            <h1 className='my-5 text-center'>Login</h1>
            <Form className="d-flex flex-column" onSubmit={loginUser}>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email"
                        placeholder="Enter email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        required />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" name="password"
                        value={formData.password}
                        onChange={handleChange} required />
                </Form.Group>

                <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" className="col-2" disabled={!isActive}>
                    Login
                </Button>

            </Form>
        </>
    );
}

export default Register;